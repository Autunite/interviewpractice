# data structures for linked lists
# trees, stacks, queues, maybe  hash table
import pdb
#linked list
class Linked_List:
   
   class Node:
      def __init__(self, *args):
         self.data = None
         self.next = None
         if args is not None:
            for arg in args:
               self.data = arg
         else:
            pass
         
         
      def __str__(self):
         if self.next is None:
            if self.data is None:
               return "'None'"
            elif type(self.data) == type(''):
               return "'"+self.data+"'"
            else:
               return str(self.data)
         else:
            if self.data is None:
               return "'None'"+", "+str(self.next)
            elif type(self.data) == type(''):
               return "'"+self.data+"'"+", "+str(self.next)
            else:
               return str(self.data)+", "+str(self.next)
            
            
      def add(self, data):
         if self.next is None:
            self.next = Linked_List.Node(data)
         else:
            self.next.add(data)
      
      def reverse(self):
         #import pdb
         #pdb.set_trace()
         
         if self is not None: 
         #self exists
            if self.next is not None:
               sublist = self.next
               if sublist.next is not None: #if self.next.next != None
                  ssublist = sublist.next
                  self.next = None
                  
                  rsublist = sublist.reverse()
                  rsublist.add(self.data)
                  return rsublist
                  
               else: #if self.next.next == None
                   self.next = None
                   rsublist = sublist.reverse()
                   rsublist.add(self.data)
                   return rsublist
               
            else: #if self.next = None
               return self
         else:
            #vibe check
            return TypeError
         
         # if self.next is None:
            # return Linked_List.Node(self.data)
         # elif self.next is not None:
            # sublist = self.next
            # if sublist.next is None:
               # sublist.next = Linked_List.Node(self.data)
               # return sublist
            # else:
               # sublist.reverse().next=Linked_List.Node(self.data)
               # return sublist
            
            
            
   
   def __init__(self, *args):
      self.topNode = None
      if args is not None:
         # try:
            # iteratorType = ( unit for unit in iterableType )
         # except TypeError as te:
            # print "not an iterable type"
            # print te
         # import pdb
         # pdb.set_trace()
         # #this attaches to the top node
         # self.insertIterable(iterable)
         for arg in args:
            self.topNode = self.Node(arg)
      else:
         self.topNode = None
   def __str__(self):
      if self.topNode is not None:
         return "["+str(self.topNode)+"]"
      else:
         return "[]"
   #insert 
   def insert(self, data ):
      pass
      
   def append(self, data):
      if self.topNode is None:
         self.topNode = self.Node(data)
      else:
         self.topNode.add(data)
      # if self.topNode is not None:
         # self.topNode.add(data)
      # else:
         # self.topNode = self.Node(data)
      
   def preappend(self, data):
      if self.topNode is None:
         self.topNode = self.Node(data)
      else:
         temp = self.topNode
         self.topNode = self.Node(data)
         self.topNode.next = temp
         
   #reverses the list (returns reverse list)
   def reverse(self):
      if self.topNode is None:
         return TypeError
         #return []
      elif self.topNode.next is None:
         myNode = self.topNode
         return 
         #return [self.topNode.data]
      else: #top.next != None
         tmpNode = self.topNode
         datatmp = tmpNode.data
         self.topNode = tmpNode.reverse()
         
         
      
   def insertIterable(self, iterableType ):
      for unit in iterableType:
         self.append(unit)

#tree
class Binary_Tree:
   
   class InvalidOperationError(Exception):
      #message must always be a string
      def __init__(self, msg):
         self.msg = msg
         
      def __str__(self):
         return self.msg
   
   class Node:
      def __init__(self, data=None):
         self.data = data #should never be null
         self.left = None
         self.right = None
         
      def __str__(self):
         treeList = self.inorder()
         
         return str(treeList)
         
      #insert
      def insert(self, data):
         if self.data is None:
            self.data = data
         if data < self.data:
            if self.left is None:
               self.left = Binary_Tree.Node(data)
               return
            self.left.insert( data )
         if data > self.data:
            if self.right is None:
               self.right = Binary_Tree.Node(data)
               return
            self.right.insert( data )
         else:
            return #implied equals
      #traverse print
      #inorder
      def inorder(self):
         #returns list
         listx = []
         #check left
         if self.left is not None:
            listx = self.left.inorder() + listx
         #check data
         if self.data is not None:
            listx = listx+[self.data]
         #check right
         if self.right is not None:
            listx = listx+self.right.inorder()
         
         return listx
         
      #helper for delete
      def numChildren(self):
         if (self.left and self.right) is not None:
              return 2
                  
         #one child
         elif( bool(self.left) ^ bool(self.right)) is not None:
            return 1
                  
                  
         #no children
         else:
            return 0
         
      def minValueNode(self):
         if self.data == None:
            return Binary_Tree.Node(None)
         if self.left is None:
            return Binary_Tree.Node(self.data)
         else:
            return self.left.minValueNode()
         
      def copy(self):
         newNode = Binary_Tree.Node(self.data)
         if self.left is not None:
            newNode.left = self.left.copy()
         if self.right is not None:
            newNode.right = self.right.copy()
         return newNode
         
         
      #returns root of new tree
      def delete(self, key ):
         if self.data is None:
            if key == None:
               return None
            return self
         
         if key == self.data:
            flag = self.numChildren()
            
            if flag is not None:
               if flag == 0:
                  return None
               elif flag == 1 :
                  if self.left is not None:
                     return self.left
                  else:
                     return self.right
               elif flag == 2 :
                  minValNode = self.right.minValueNode()
                  self.data = minValNode.data
                  self.right = self.right.delete(self.data)
                  return self
               else:
                  return self
            else:
               return None
            
         else:
            if key < self.data :
               if self.left is not None:
                  self.left = self.left.delete(key)
               else:
                  return self
            else:
               if self.right is not None:
                  self.right = self.right.delete(key)
               else:
                  return self
            return self
      
      #checks if tree is balanced. True if yes, False if not
      def isBalanced(self):
         #check if the abs of the difference in height
         #between the two sub-trees is greater than 1
         leftHeight = 0
         rightHeight = 0
         leftBalBool = True
         rightBalBool = True
         selfBalBool = False
         
         if self.left is not None:
            leftHeight = self.left.height()
            leftBalBool = self.left.isBalanced()
            
         if self.right is not None:
            rightHeight = self.right.height()
            rightBalBool = self.right.isBalanced()
            
         if abs(rightHeight - leftHeight) >1 :
            return False
         else:
            selfBalBool = True
         return (rightBalBool and leftBalBool and selfBalBool)
      
      #returns the number of nodes in the binary tree
      def numNodes( self ):
         count = 0
         if self.data is not None :
            count+=1
            
         if self.left is not None :
            count = count + self.left.numNodes()
            
         if self.right is not None :
            count = count + self.right.numNodes()
         return count
      
      #does a rotate with the root and the pivot to the root's left
      #returns a tree.
      #returns error if not possible
      def rotateRight(self):
         try:
            
            if self.left is None:
               msg = "Cannot right rotate this Tree!, Needs sub Node"
               raise Binary_Tree.InvalidOperationError(msg)
               return
            else:
               #data saves
               oldRootData = self.data
               oldRootsRight = self.right
               oldPivotData = self.left.data
               oldPivot = self.left
               oldPivotsRight = oldPivot.right
               
               
               #turn root's data into pivot's data
               self.data = oldPivotData
               self.left = oldPivot.left
               self.right = Binary_Tree.Node(oldRootData)
               self.right.left = oldPivotsRight
               self.right.right = oldRootsRight
               return
         except Binary_Tree.InvalidOperationError as invOpErr :
            print invOpErr
            return
      
      #does a rotate with the root and the pivot to the root's right
      #returns a tree.
      #returns error if not possible
      def rotateLeft(self):
         try:
            
            if self.right is None:
               msg = "Cannot left rotate this Tree!, Needs sub Node"
               raise Binary_Tree.InvalidOperationError(msg)
               return
            else:
               #data saves
               oldRootData = self.data
               oldRootsLeft = self.left
               oldPivotData = self.right.data
               oldPivot = self.right
               oldPivotsLeft = oldPivot.left
               
               
               #turn root's data into pivot's data
               self.data = oldPivotData
               self.left = Binary_Tree.Node(oldRootData)
               self.left.right = oldPivotsLeft
               self.left.left = oldRootsLeft
               self.right = oldPivot.right
               
               return
         except Binary_Tree.InvalidOperationError as invOpErr :
            print invOpErr
            return
            
      #helper function for naive balance. 
      #returns middle element (roof) of the list
      #returns the left and sub lists
      #returns nulls if the list or subLists
      #are empty
      def grabMiddle(self, treeList):
         size = len(treeList)
         if size is 0 :
            return None, [], []
         elif size is 1 :
            return treeList[0], [],[]
         elif size is 2:
            #since treeList is inOrder, middle roof of two
            #is the 2nd element,
            #first element is the left tree list
            #make the right treelist empty
            return treeList[1], [treeList[0]], []
         elif size is 3 :
            return treeList[1], [treeList[0]], [treeList[2]]
         else:
            #this is for all sizes above 3
            middleIndex = (size/2)+1
            middle = treeList[middleIndex]
            leftTreeList = treeList[:middleIndex]
            rightTreeList = treeList[middleIndex+1:]
            rightTreeList
            return middle, leftTreeList, rightTreeList

      
      #yet another helper function
      #this one turns an ordered list into a balanced tree
      @staticmethod
      def orderedListToTree(orderedList):
         import pdb
         listLen = len(orderedList)
         newTree = Binary_Tree.Node()
         if listLen is 0 :
            return newTree
         elif listLen is 1 :
            newTree.data = orderedList[0]
            return newTree
         elif listLen is 2 :
            newTree.data = orderedList[1]
            newTree.left = Binary_Tree.Node(orderedList[0])
            return newTree
         else:
            #implicit catch all for 3 or greater
            middle, leftList, rightList = newTree.grabMiddle(orderedList)
            newTree.data = middle
            newTree.left = newTree.orderedListToTree(leftList)
            newTree.right = newTree.orderedListToTree(rightList)
            return newTree

      
      #dumb naive implementation of balance 
      #function, may return to do better with
      #returns a new tree
      def balance(self):
      
         
         if self.data is None:
            #if data is none, then the tree has no data
            return Binary_Tree.Node()
         elif self.isBalanced():
            #this will supposedly catch trees that are of size
            #one or two implicitly, but then it screws with the recursion
            return self
         else:
            #three elements or more
            treeList = self.inorder()
            treeLen = len(treeList)
            newTree = Binary_Tree.Node()
            element, leftTreeList, rightTreeList = self.grabMiddle(treeList)
            newTree.data = element
            
            newTree.left = self.orderedListToTree(leftTreeList)
            newTree.right = self.orderedListToTree(rightTreeList)
            
            
            return newTree
            #hmm..
            
         

      #preorder
      def preorder(self):
         listx = []
         #check root
         listx = listx+[self.data]
         
         #check left
         if self.left is not None:
            listx = listx + self.left.preorder()
         
         #check right
         if self.right is not None:
            listx = listx + self.right.preorder()
            
         return listx
      
      #postoder
      def postorder(self):
         listx = []
         
         #check left
         if self.left is not None:
            listx =  self.left.postorder()+listx
         
         #check right
         if self.right is not None:
            listx = listx + self.right.postorder()
         
         #check root
         listx = listx + [self.data]
         
         return listx
      
      #height
      def height(self):
         numR = 0
         numL = 0
         num = 0
         
         if self.left is not None:
            numL = 1 + self.left.height()
         if self.right is not None:
            numR = 1 + self.right.height()
         return max(numR,numL)
      
      
      
      
   
   def __init__(self, data):
      self.root = Binary_Tree.Node(data)
      

#stack

#queue
