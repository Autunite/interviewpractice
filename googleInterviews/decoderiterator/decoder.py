import pdb

class DecoderIterator:
   
   def __init__( self, list=[] ):
      self.list = list
      self.arrLen = len(list)
      
      try:
         if self.arrLen % 2 or ( self.arrLen == 0 ):
            raise ValueError
         else :
            self.encodedList = list
      except ValueError :
         print("Encoded lists by definition must have an even num of indices")
         #os exit 2?
         
         return None
      
      self.indexTracker  = 0
      self.inRepCounter  = self.list[ self.indexTracker ]
      self.repeatedNum = self.list[ self.indexTracker+1 ]
      return None
      
   def __iter__( self ):
      #supposedly all I need to do is return self
      return self
      
   def next( self ):
      #if inRepCounter is 0
      if not self.inRepCounter :
         
         
         #check if you're at the end of the arr
         if self.indexTracker == self.arrLen-2:
            raise StopIteration
         else:
            #not at end of arr
            #increment before calling next
            self.__update()
            decodedInt = next( self )
      else :
         self.inRepCounter -= 1
         decodedInt = self.repeatedNum
      
      return decodedInt
      
   def __update( self ):
      self.indexTracker += 2
      self.inRepCounter  = self.list[ self.indexTracker ]
      self.repeatedNum = self.list[ self.indexTracker+1 ]