import decoder, pdb



def main():
   
   print("Test, iterator")
   goodList = [1,7,0,8,3,6]
   badList1 = [1,2,3]
   badList2 = [3]
   badList3 = [3,2,4,6,5,1,0,1]
   
   #decodeGen = decoder.DecoderIterator
   
   goodDecoder = decoder.DecoderIterator(goodList)
   badDecoder = decoder.DecoderIterator( [] )
   #decoderIter =iter(goodDecoder)
   #write a generator in the future for this iterator.
   
   #pdb.set_trace()
   for x in goodDecoder:
      print x

if __name__ == '__main__':
   main()