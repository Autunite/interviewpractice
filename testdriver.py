#Here's your sandbox playground
import dataS
import pdb, string

def insert_helper( num, tree_root=dataS.Binary_Tree.Node()):
   print "Inserting " +str(num)
   print tree_root.inorder()
   tree_root.insert(num)
   print tree_root.inorder()
   
   
def delete_helper(num, tree_root=dataS.Binary_Tree.Node()):
   print "Deleting " +str(num)
   print tree_root.inorder()
   tree_root = tree_root.delete(num)
   print tree_root.inorder()
   
def balanceCheckerHelper(tree=dataS.Binary_Tree.Node()):
   origBalState = tree.isBalanced()
   origTreeOrder = tree.inorder()
   
   newTree = tree.balance()
   newBalState = newTree.isBalanced()
   newTreeOrder = newTree.inorder()
   print "Old Tree Bal State"
   print origBalState
   print "Is the new tree Balanced?"
   print newBalState
   print "Is the new Tree in the same order as the last?"
   if origTreeOrder == newTreeOrder :
      print "Success! Yes"
   else :
      print "Failure! No"
      
   print "Old Order:"
   print origTreeOrder
   print "New Order:"
   print newTreeOrder
   
   return NotImplementedError
   
def treeBuilder(size=0, balanceBool=True ):
   print "Tree builder"
   tree=dataS.Binary_Tree.Node()
   treeList = range(size)
   #returns a tree of size n, and you can choose whether to balance it or not
   if ( not  balanceBool ) and ( size >= 3 ) :
      print "Building non balanced tree"
      for item in treeList :
         tree.insert(item)
   else:
      print "building balanced tree"
      tree = tree.orderedListToTree(treeList)
   print "Is Tree Balanced? : "+str(tree.isBalanced())
   print "printing tree:"
   print tree
   return tree
   
   
def treeRotCheckerHelper(tree=dataS.Binary_Tree.Node(), direction='right'):
   try:
      if not (tree.numNodes() > 1 ) :
         msg = 'Needs to have two nodes minimum'
         raise dataS.Binary_Tree.InvalidOperationError(msg)
         return
         
      oldOrder = tree.inorder()
      oldRootsData = tree.data
      oldPivotsData = None
      newRootsData = None
      newPivotsData = None
        
      if type(direction) is not type('  '):
         raise TypeError
      else:
            direction = direction.lower()
            direction = direction.strip(string.punctuation)
            if direction == "left" :
               oldPivotsData = tree.right.data
               print "rotating left"
               tree.rotateLeft()
               newPivotsData = tree.left.data
               newRootsData = tree.data
            elif direction == "right":
               oldPivotsData = tree.left.data
               print "rotating right"
               tree.rotateRight()
               newPivotsData = tree.right.data
               newRootsData = tree.data
            else:
               raise ValueError
   except TypeError as typeErr:
      print "needs to be a string"
   except ValueError as valErr:
        print "direction needs to be 'left' or 'right' "
   except dataS.Binary_Tree.InvalidOperationError as invValOpErr :
      print invValOpErr
      
   newOrder = tree.inorder()
   
   if not (newOrder == oldOrder):
      print "Something went wrong, the order changed"
      print "Old Order:"
      print oldOrder
      print "New Order"
      print newOrder
   else:
      print "Success"
      print "Old Order:"
      print oldOrder
      print "New Order"
      print newOrder
      
   if newRootsData == oldPivotsData :
      print "Sucess, newRootsdata is old Pivots data"
   else:
      print "Something went wrong, newRootsData is not Old pivots data"
      
   if newPivotsData == oldRootsData :
      print "Sucess, newPivotsData is oldRootsData"
   else:
      print "Something went wrong, newPivotsData is not oldRootsData"
      
   print "Old Roots data is"
   print oldRootsData
   print "New roots data is "
   print newRootsData

   print "Old Pivots data is"
   print oldPivotsData
   print "New Pivots data is"
   print newPivotsData
   

def listToTreeHelper(list=[]):
   print "listToTreeHelper"
   print "making a tree from list:"
   print list
   treeNode = dataS.Binary_Tree.Node()
   tree = treeNode.orderedListToTree(list)
   newList = tree.inorder()
   
   if list == newList :
      print "success"
   else:
      print "failed"
   print "New list is:"
   print newList
   return tree

def main():
   print("playground")
   L1 = dataS.Linked_List()
   print L1
   L1.append('b')
   print L1
   L1.append('c')
   print L1
   L1 = dataS.Linked_List("playground")
   print L1
   str1 = "testString"
   
   
   print "making trees"
   print "bin tree y"
   y = dataS.Binary_Tree.Node()
   insert_helper(2, y)
   insert_helper(1, y)
   insert_helper(3, y)
   print "printing tree inorder"
   print y.inorder()
  
   print "bin tree x"
   x = dataS.Binary_Tree.Node()
   insert_helper(4, x)
   insert_helper(6, x)
   insert_helper(2, x)
   insert_helper(1, x)
   insert_helper(3, x)
   insert_helper(5, x)
   insert_helper(7, x)
   
   print "printing tree inorder"
   
   print x.inorder()
   
   z = dataS.Binary_Tree.Node()
   print "Building Z"
   insert_helper(15, z)
   insert_helper(10, z)
   insert_helper(8, z)
   insert_helper(13, z)
   insert_helper(12, z)
   insert_helper(17, z)
   insert_helper(16, z)
   
   insert_helper(18, z)
   
   print "Printing Z in order"
   print z.inorder()
   
   print "Is z balanced?"
   print str(z.isBalanced())
   
   print "Deleting 18 then 16 unbalance the tree"
   z = z.delete(18)
   print str(z.isBalanced())
   
   z = z.delete(16)
   print z.inorder()
   print "the root of this tree is "+str(z.data)
   print str(z.isBalanced())
   print""
   print"Rotations:"
   
   print "Building n"
   n = dataS.Binary_Tree.Node()
   n.insert(16)
   n.insert(17)
   n.insert(14)
   n.insert(13)
   n.insert(15)
   print "Printing n in order"
   print n.inorder()
   
   
   
   
   #pdb.set_trace()
   treeRotCheckerHelper(n,'right')
   treeRotCheckerHelper(n,'left')
   
   
   print n.inorder()
   
   list = [1,2,3]
   newtree = listToTreeHelper(list)
   list = [1,2]
   newtree2 = listToTreeHelper(list)
   list = [1]
   newtree3 = listToTreeHelper(list)
   
   list = []
   newtree4 = listToTreeHelper(list)
   list = range(4)
   newtree = listToTreeHelper(list)
   list = range(6)
   newtree = listToTreeHelper(list)
   
   balTree = treeBuilder(8, True )
   nonBalTree = treeBuilder(8, False)
   
   balanceCheckerHelper(balTree)
   
   balanceCheckerHelper(nonBalTree)
   
   pdb.set_trace()
   
   #Deep Copy, Deletions and balancing
   #delete from  each of the four zones
   #create an original tree and make a deep copy 4 times?

   
if __name__ == '__main__':
   main()
